<?php
add_action( 'wp_enqueue_scripts', 'wpm_enqueue_styles' );
function wpm_enqueue_styles(){
wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
// MINIMUM CART AMOUNT FOR CHECKOUT
add_action( 'woocommerce_check_cart_items', 'required_min_cart_subtotal_amount' );
function required_min_cart_subtotal_amount() {
    $zone = WC_Shipping_Zones::get_zone_by ( 'zone_name', "1" . 'zone_name', "2" . 'zone_name' , "3" );

    // Only run it in  Cart or Checkout pages
    if( is_cart() || ( is_checkout() && ! is_wc_endpoint_url() ) ) {
        // Get cart shipping packages
        $shipping_packages =  WC()->cart->get_shipping_packages();

        // Get the WC_Shipping_Zones instance object for the first package
        $shipping_zone = wc_get_shipping_zone( reset( $shipping_packages ) );
        $zone_id   = $shipping_zone->get_id(); // Get the zone ID
        $zone_name = $shipping_zone->get_zone_name(); // Get the zone name

        // Total (before taxes and shipping charges)
        $total = WC()->cart->subtotal;

         // HERE Set minimum cart total amount (for Zone 1,2,3 and for other zones)
       // $min_total = $zone_name == ״1״ ? 99.99 : 200.0;
$min_total = 200;
if  ($zone_name == "France") $min_total = 0;
if ($zone_name == "Emplacements non couverts par vos autres zones") $min_total = 30;

        // Add an error notice is cart total is less than the minimum required
   if( $total <= $min_total  ) {
            // Display an error message
 wc_add_notice(   sprintf(
                __("minimum order is - %s "),
                wc_price( $min_total)
            )  , 'error' );
        }
    }
}
